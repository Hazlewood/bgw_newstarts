﻿Imports excel = Microsoft.Office.Interop.Excel
Imports System.DirectoryServices
Public Class common
#Region "Definitions"
    Public Declare Function AllocConsole Lib "kernel32.dll" () As Boolean
    Public Declare Function AttachConsole Lib "kernel32.dll" (ByVal dwProcessId As Int32) As Boolean
    Public Declare Function FreeConsole Lib "kernel32.dll" () As Boolean
    Public Shared stopwatch As New Stopwatch
    Public Shared DPMonitor As Boolean = True
    Public Shared DumpExcel As Boolean = True
    Public Shared Dev As Boolean = My.Settings.Dev
    '
    Public Shared PUID As String = Nothing
    Public Shared m_NewStartArray(10, 20) As String
    Public Shared workingDomain As String = Nothing
    Public Shared mycounter As Integer = Nothing
    Public Shared m_Environment As String = Nothing
    Public Shared myArraylist As ArrayList = Nothing
    'convertdate 
    Public Shared outputFileDate As String = Nothing
    Public Shared filename As String = Nothing
    Public Shared strList As New List(Of String)
    'Audit Trail
    Public Shared auditFileName As String = Nothing
    '
    Public Shared m_RACFIDList As New ArrayList
    Public Shared m_foundlist As New ArrayList
    '
    Public Shared workbook As Object = Nothing
    Public Shared worksheet As Object = Nothing
    '
    Public Shared NewStartTable As DataTable
    '
    Public Shared usertypeArr(10, 3)

    Public Shared selectedfile As String = ""
    Public Shared ViaDomainController As String = "ukncdavia811"
    Public Shared ViaDomainName As String = "DC=via,DC=novonet"
    Public Shared ViaConnectionString As String = "LDAP://" & ViaDomainController & "/" & ViaDomainName
    '
    Public Shared AvivagroupDomainController As String = "ukncdavia001"
    Public Shared AvivagroupDomainName As String = "DC=avivagroup,DC=com"
    Public Shared AvivagroupConnectionString As String = "LDAP://" & AvivagroupDomainController & "/" & AvivagroupDomainName

    'Public Shared GlobalDomainController As String = ""
    Public Shared GlobalDomanName As String = "DC=Global,DC=novonet"
    Public Shared GlobalConnectionString As String = "LDAP://" & GlobalDomanName

    'Public Shared iehibdomDomainController As String = "IEALAD1"
    Public Shared iehibdomDomainName As String = "DC=iehibdom,DC=com"
    Public Shared iehibdomConnectionString As String = "LDAP://" & iehibdomDomainName

    Public Shared iealdomDomainController As String = "IEALAD1"
    Public Shared iealdomDomainName As String = "DC=iealdom,DC=com"
    Public Shared iealdomConnectionString As String = "LDAP://" & iealdomDomainController & "/" & iealdomDomainName
#End Region
End Class
