﻿Public Class ArgumentType
    'class for passing arguments to
    'backgroundworker
    '
    '** usage
    'dim fname as string = [first name]
    'dim lname as string = [last name]
    'dim country as string = [country]
    'dim args as ArgumentType
    'args.fname = fname.trim
    'args.lname = lname.trim
    'args.country = country.trim
    '
    ' Pass argument to worker thread
    'bgw1.RunWorkerAsync(args)
    '
    'Private Sub bgw1_DoWork(sender As Object, e As DoWorkEventArgs)
    '' Access variables through e
    'Dim args As ArgumentType = e.Argument
    '_accessLayer.sendImg(args._User, args._Action, args._Flag)
    'End Sub
    '
    Public fName As String
    Public lName As String
    Public country As String
End Class
