﻿Imports BGW_Test.common
Imports System.IO
Imports System.DirectoryServices
Imports vb = Microsoft.VisualBasic
Imports System.ComponentModel

Public Class frmMain
    Dim bgwRACFID As BackgroundWorker = New BackgroundWorker
    

    Private Sub Form1_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        ComBoxCountry.Items.Add("Ireland")
        'ComBoxCountry.Items.Add("UK")
        Me.ComBoxCountry.SelectedIndex = 0

        'load bad word list
        Dim myList As String = My.Resources.badwordlist
        '
        '
        Using reader As New StringReader(myList)
            While reader.Peek() <> -1
                strList.Add(reader.ReadLine())
            End While
        End Using

    End Sub
    Private Sub btnGo_Click(sender As System.Object, e As System.EventArgs) Handles btnGo.Click
        Dim result As String = String.Empty

        splitname(Me.tbName.Text.Trim)
        Cursor = Cursors.WaitCursor
        result = manualPUIDgeneration(Me.tbFname.Text.Trim, Me.tbLname.Text.Trim)
        Me.tbeMail.Text = checkForValidEmail(Me.tbFname.Text.Trim, Me.tbLname.Text.Trim, "Ireland")
        Cursor = Cursors.Default

    End Sub
    Public Function splitname(ByVal name As String) As String
        Dim strArr As Array
        Dim fname, lname As String

        strArr = Split(name, " ")
        fname = strArr(0)
        lname = strArr(1)
        Me.tbFname.Text = fname
        Me.tbLname.Text = lname
        Return fname
    End Function

    Public Function manualPUIDgeneration(ByVal fname As String, ByVal lname As String) As String
        Dim result As ArrayList
        'Dim emresult As String
        Dim country As String = ""

        'generate possible PUID's
        'unique ID placed in common.PUID
        result = generatePUID(fname, lname)
        tbRACFID.Text = common.PUID

        If Me.ComBoxCountry.SelectedIndex = 0 Then
            country = "Ireland"
        ElseIf Me.ComBoxCountry.SelectedIndex = 1 Then
            country = "UK"
        End If
        'generate email address
        'emresult = generateeMailAddress(fname, lname, country)
        'emresult = checkForValidEmail(fname, lname, country)
        'tbeMail.Text = emresult
        Return Nothing
    End Function
    Public Shared Function ADC_search4RACFID(ByVal RACFID As String, ByVal connectionstring As String) As Boolean
        'search Domain for RACFID
        'return true if found, False if not
        Dim found As Boolean = False
        Dim direntry As New DirectoryEntry(connectionstring)
        Dim searcher As New DirectorySearcher(direntry)
        'container for all results
        'Dim AllResults As SearchResultCollection
        'container for individual results
        Dim result As SearchResult
        'check we have a value in RACFID
        searcher.Filter = "(&(objectclass=user)(CN=" & RACFID & "))"
        '
        'perform the search
        'AllResults = searcher.FindAll()
        result = searcher.FindOne
        '
        If result Is Nothing Then
            Return False
        Else
            Return True
        End If
    End Function

    Public Function generatePUID(ByVal fName As String, ByVal lName As String, Optional ByVal cnt As Integer = 6) As ArrayList
        'generates a list of potential ID's
        'based on first and last names
        'returns m_PUID as an arraylist
        Dim badword As String = ""
        Dim m_PUID As New ArrayList
        Dim PUID As String = ""
        Dim UKresult As Boolean = False
        Dim IREresult As Boolean = False
        Dim GlobalResult As Boolean = False
        '
        Dim fnameLength As Integer = Len(fName)
        Dim lNameLength As Integer = Len(lName)
        'AllocConsole()
        Try
tryagain:
            common.PUID = ""
            'remove apostrophies
            'Console.WriteLine(header.Trim( { " "c, "*"c, "."c } ))
            'phrase = phrase.Replace(",", "")
            fName = fName.Replace("'", "")
            lName = lName.Replace("'", "")
            'check length of Last Name
            If lNameLength < 5 Then
                PUID = vb.Left(lName, Len(lName)) & vb.Left(fName, (5 - Len(lName)))
                badword = checkbadwords(PUID)
                If Not String.IsNullOrEmpty(badword) Then
                    'remove the offending word
                    Dim foundS1 As Integer = lName.IndexOf(badword)
                    Dim foundS2 As Integer = Len(badword)
                    ' remove the bad word
                    If Len(lName) - foundS2 <= 0 Then
                        'can't remove bad word - flag
                        writeAudit(auditFileName, "Unsuitable RACFID")
                        Return Nothing
                        Exit Function
                    End If
                    lName = lName.Remove(foundS1 + 1, foundS2)
                    GoTo tryagain
                End If
                m_PUID.Add(UCase(PUID))
            ElseIf lNameLength >= 5 Then
                PUID = vb.Left(lName, 5) & vb.Left(fName, 1)
                badword = checkbadwords(PUID)
                If Not String.IsNullOrEmpty(badword) Then
                    'remove the offending letters
                    Dim foundS1 As Integer = lName.IndexOf(badword)
                    Dim foundS2 As Integer = Len(badword)
                    'If foundS1 <> foundS2 And foundS1 >= 0 Then
                    ' remove the middle name, identified by finding the spaces in the middle of the name...    
                    lName = lName.Remove(foundS1 + 1, foundS2)
                    GoTo tryagain
                End If
            End If
            If Len(PUID) < 4 Then
                writeAudit(auditFileName, "Name too short")
                'Me.btnStart.Enabled = False
                'Me.btnClear.Enabled = True
                Return Nothing
                Exit Function
            End If
            m_PUID.Add(UCase(PUID))
            For x = 1 To 9
                m_PUID.Add(UCase(PUID) + Trim(Val(x)))
            Next
            'search for an unused PUID in AG
            For x = 0 To 9
                'check UK
                UKresult = ADC_search4RACFID(UCase(m_PUID(x)), AvivagroupConnectionString)
                If UKresult = False Then
                    'check Ireland
                    IREresult = ADC_search4RACFID(UCase(m_PUID(x)), iehibdomConnectionString)
                    If IREresult = False Then
                        'check Global
                        GlobalResult = ADC_search4RACFID(UCase(m_PUID(x)), GlobalConnectionString)
                        'GlobalResult = ADC_search4RACFID(** REAL RACFID **, GlobalConnectionString)
                        If GlobalResult = False Then
                            common.PUID = m_PUID(x)
                            Return m_PUID
                            Exit Function
                        End If
                    End If
                End If
            Next
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
        Return m_PUID
    End Function
    Public Function checkbadwords(ByVal myPUID As String) As String
        Dim badWord As String = ""
        For Each element As String In strList
            If myPUID.Contains(UCase(element)) Then
                badWord = UCase(element)
                Exit For
            End If
        Next
        Return badWord
    End Function
    Public Function generateeMailAddress(ByVal fname As String, ByVal lname As String, ByVal country As String) As String
        '******************************************************
        '21/2/2014
        'Les Tomkinson for Aviva plc
        'generateEmailAddress
        'INPUT
        'fName (First Name) String
        'lName (Last Name) String
        'country (Country UK / IRELAND) String
        'OUTPUT
        'email address as string
        '******************************************************
        Dim eMail As String = ""
        Dim company As String = ""
        If UCase(country) = "UK" Then
            company = "aviva.co.uk"
            'ElseIf rbAUK.Checked And cboxDotCom.Checked = True Then
            'company = "aviva.com"
        ElseIf UCase(country) = "IRELAND" Then
            company = "aviva.ie"
        End If
        eMail = LCase(Trim(fname)) & "." & LCase(Trim(lname)) & "@" & company
        Return eMail
    End Function
    Public Function checkForValidEmail(ByVal Fname As String, ByVal Lname As String, ByVal Country As String) As String
        'function needs to check AG and IEHibdom
        Dim cntr As Integer = 1
        Dim company As String = ""

        Select Case Country
            Case Is = "UK"
                company = "aviva.co.uk"
            Case Is = "Ireland"
                company = "aviva.ie"
        End Select

        Dim peMail As String = LCase(Fname & "." & Lname & "@" & company)
        Dim myeMail As String = ""
        Dim validEmail As String = ""
        '
        Dim AGde As New DirectoryEntry
        AGde.Path = common.AvivagroupConnectionString
        Dim AGds As New DirectorySearcher
        Dim AGresult As SearchResult
        '
        Try
            AGds.PropertiesToLoad.Add("mail")
            AGds.PropertiesToLoad.Add("SAMAccountname")
tryagain:
            AGds.Filter = "mail=" & peMail
            '
            AGresult = AGds.FindOne
            If Not IsNothing(AGresult) Then
                'found this email, so try again
                peMail = LCase(Fname & "." & Lname & Trim(Val(cntr) & "@" & company))
                cntr += 1
                If cntr > 15 Then
                    peMail = "**** Manual Check Required ****"
                    Return peMail
                    Exit Function
                End If
                GoTo tryagain
            End If
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
        'utilities.updateeMail(peMail)
        Return peMail
    End Function
    Public Function writeAudit(ByVal fileName As String, ByVal _line As String) As Boolean
        Dim filewritten As Boolean
        Dim objWriter As New System.IO.StreamWriter(fileName, True)
        objWriter.Write(_line, True)
        objWriter.Close()
        filewritten = True
        Return filewritten
    End Function
End Class
