﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmMain
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.lblLname = New System.Windows.Forms.Label()
        Me.lblFhame = New System.Windows.Forms.Label()
        Me.tbLname = New System.Windows.Forms.TextBox()
        Me.tbFname = New System.Windows.Forms.TextBox()
        Me.tbName = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.btnGo = New System.Windows.Forms.Button()
        Me.BackgroundWorker1 = New System.ComponentModel.BackgroundWorker()
        Me.tbRACFID = New System.Windows.Forms.TextBox()
        Me.tbeMail = New System.Windows.Forms.TextBox()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.ComBoxCountry = New System.Windows.Forms.ComboBox()
        Me.tbProgress = New System.Windows.Forms.TextBox()
        Me.SuspendLayout
        '
        'lblLname
        '
        Me.lblLname.AutoSize = true
        Me.lblLname.Location = New System.Drawing.Point(373, 71)
        Me.lblLname.Name = "lblLname"
        Me.lblLname.Size = New System.Drawing.Size(27, 13)
        Me.lblLname.TabIndex = 30
        Me.lblLname.Text = "Last"
        '
        'lblFhame
        '
        Me.lblFhame.AutoSize = true
        Me.lblFhame.Location = New System.Drawing.Point(227, 71)
        Me.lblFhame.Name = "lblFhame"
        Me.lblFhame.Size = New System.Drawing.Size(26, 13)
        Me.lblFhame.TabIndex = 29
        Me.lblFhame.Text = "First"
        '
        'tbLname
        '
        Me.tbLname.Enabled = false
        Me.tbLname.Location = New System.Drawing.Point(406, 67)
        Me.tbLname.Name = "tbLname"
        Me.tbLname.Size = New System.Drawing.Size(100, 20)
        Me.tbLname.TabIndex = 26
        Me.tbLname.TabStop = false
        '
        'tbFname
        '
        Me.tbFname.Enabled = false
        Me.tbFname.Location = New System.Drawing.Point(259, 67)
        Me.tbFname.Name = "tbFname"
        Me.tbFname.Size = New System.Drawing.Size(100, 20)
        Me.tbFname.TabIndex = 25
        Me.tbFname.TabStop = false
        '
        'tbName
        '
        Me.tbName.Location = New System.Drawing.Point(66, 70)
        Me.tbName.Name = "tbName"
        Me.tbName.Size = New System.Drawing.Size(155, 20)
        Me.tbName.TabIndex = 27
        '
        'Label2
        '
        Me.Label2.AutoSize = true
        Me.Label2.Location = New System.Drawing.Point(25, 74)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(35, 13)
        Me.Label2.TabIndex = 28
        Me.Label2.Text = "Name"
        '
        'btnGo
        '
        Me.btnGo.Location = New System.Drawing.Point(66, 199)
        Me.btnGo.Name = "btnGo"
        Me.btnGo.Size = New System.Drawing.Size(75, 23)
        Me.btnGo.TabIndex = 31
        Me.btnGo.Text = "Go"
        Me.btnGo.UseVisualStyleBackColor = true
        '
        'BackgroundWorker1
        '
        Me.BackgroundWorker1.WorkerReportsProgress = true
        Me.BackgroundWorker1.WorkerSupportsCancellation = true
        '
        'tbRACFID
        '
        Me.tbRACFID.Location = New System.Drawing.Point(259, 110)
        Me.tbRACFID.Name = "tbRACFID"
        Me.tbRACFID.Size = New System.Drawing.Size(155, 20)
        Me.tbRACFID.TabIndex = 32
        '
        'tbeMail
        '
        Me.tbeMail.Location = New System.Drawing.Point(259, 136)
        Me.tbeMail.Name = "tbeMail"
        Me.tbeMail.Size = New System.Drawing.Size(155, 20)
        Me.tbeMail.TabIndex = 33
        '
        'Label13
        '
        Me.Label13.AutoSize = true
        Me.Label13.Location = New System.Drawing.Point(336, 15)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(43, 13)
        Me.Label13.TabIndex = 35
        Me.Label13.Text = "Country"
        '
        'ComBoxCountry
        '
        Me.ComBoxCountry.FormattingEnabled = true
        Me.ComBoxCountry.Location = New System.Drawing.Point(385, 12)
        Me.ComBoxCountry.Name = "ComBoxCountry"
        Me.ComBoxCountry.Size = New System.Drawing.Size(121, 21)
        Me.ComBoxCountry.TabIndex = 34
        '
        'tbProgress
        '
        Me.tbProgress.Location = New System.Drawing.Point(259, 162)
        Me.tbProgress.Name = "tbProgress"
        Me.tbProgress.Size = New System.Drawing.Size(155, 20)
        Me.tbProgress.TabIndex = 36
        '
        'frmMain
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6!, 13!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(563, 314)
        Me.Controls.Add(Me.tbProgress)
        Me.Controls.Add(Me.Label13)
        Me.Controls.Add(Me.ComBoxCountry)
        Me.Controls.Add(Me.tbeMail)
        Me.Controls.Add(Me.tbRACFID)
        Me.Controls.Add(Me.btnGo)
        Me.Controls.Add(Me.lblLname)
        Me.Controls.Add(Me.lblFhame)
        Me.Controls.Add(Me.tbLname)
        Me.Controls.Add(Me.tbFname)
        Me.Controls.Add(Me.tbName)
        Me.Controls.Add(Me.Label2)
        Me.Name = "frmMain"
        Me.Text = "Form1"
        Me.ResumeLayout(false)
        Me.PerformLayout

End Sub
    Friend WithEvents lblLname As System.Windows.Forms.Label
    Friend WithEvents lblFhame As System.Windows.Forms.Label
    Friend WithEvents tbLname As System.Windows.Forms.TextBox
    Friend WithEvents tbFname As System.Windows.Forms.TextBox
    Friend WithEvents tbName As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents btnGo As System.Windows.Forms.Button
    Friend WithEvents BackgroundWorker1 As System.ComponentModel.BackgroundWorker
    Friend WithEvents tbRACFID As System.Windows.Forms.TextBox
    Friend WithEvents tbeMail As System.Windows.Forms.TextBox
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents ComBoxCountry As System.Windows.Forms.ComboBox
    Friend WithEvents tbProgress As System.Windows.Forms.TextBox

End Class
